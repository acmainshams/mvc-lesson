import 'package:flutter/material.dart';
import 'package:gmailtest/controller/controller.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class StartScreen extends StatefulWidget {
  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends StateMVC<StartScreen> {
  TextEditingController number1 = new TextEditingController();
  TextEditingController number2 = new TextEditingController();

  Controller _con;

  _StartScreenState() : super(Controller()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 80,
            ),
            Container(
              child: Text(_con.counter.toString()),
            ),
            RaisedButton(
                child: Text("Increase"),
                onPressed: () {
                  _con.incrementCounter();
                  setState(() {});
                }),
            RaisedButton(
                child: Text("Decrease"),
                onPressed: () {
                  _con.decCounter();
                  setState(() {});
                }),
            TextField(
              controller: number1,
            ),
            TextField(
              controller: number2,
            ),
            Text(_con.multiple(55, 2).toString())
          ],
        ),
      ),
    );
  }
}
