class Model {
  static int get counter => _counter;
  static int _counter = 0;
  static int incrementCounter() => ++_counter;
  static int decCounter() => --_counter;
  static int mul(int x, int y) {
    return x * y;
  }
}
