import 'package:gmailtest/models/model.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class Controller extends ControllerMVC {
  int get counter => Model.counter;
  void incrementCounter() => Model.incrementCounter();
  void decCounter() => Model.decCounter();
  int multiple(int x, int y) => Model.mul(x, y);
}
